#!/usr/bin/python3
from math import sin, cos, sqrt, tan, pi


class Vector:
    def __init__(self, x=0.0, y=0.0, z=0.0, w=0.0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __str__(self):
        return ('x: {:-10.6} y: {:-10.6} z: {:-10.6} w: {:-10.6}'.format(
            self.x, self.y, self.z, self.w))

    def __add__(self, b):
        return (Vector(self.x + b.x, self.y + b.y, self.z + b.z, self.w + b.w))

    def __sub__(self, b):
        return (Vector(self.x - b.x, self.y - b.y, self.z - b.z, self.w - b.w))

    def __mul__(self, b):
        return (Vector(self.x * b.x, self.y * b.y, self.z * b.z, self.w * b.w))

    def __matmul__(self, b):
        return (b * self)

    def __div__(self, b):
        return (Vector(self.x / b.x, self.y / b.y, self.z / b.z, self.w / b.w))

    def __mod__(self, b):
        return (Vector(self.x % b.x, self.y % b.y, self.z % b.z, self.w % b.w))

    def __lt__(self, b):
        return (self.len() < b.len())

    def __gt__(self, b):
        return (self.len() > b.len())

    def __eq__(self, b):
        if self.x == b.x and self.y == b.y and self.z == b.z and self.w == b.w:
            return (True)
        return (False)

    def __neq__(self, b):
        return not self == b

    def __getitem__(self, x):
        return (self.array()[x])

    def __setitem__(self, key, value):
        if not isinstance(value, float):
            raise(TypeError(value))
        if key == 0:
            self.x = value
        elif key == 1:
            self.y = value
        elif key == 2:
            self.z = value
        elif key == 3:
            self.w = value
        else:
            raise(IndexError(key))

    def __ne__(self, b):
        return (not self == b)

    def copy(self):
        return (Vector(self.x, self.y, self.z, self.w))

    def len(self):
        return (sqrt(self.dot(self)))

    def disance(self, vec):
        """
        return the distance between self and vec
        """
        dx = (vec.x - self.x) * (vec.x - self.x)
        dy = (vec.y - self.y) * (vec.y - self.y)
        dz = (vec.z - self.z) * (vec.z - self.z)
        return (sqrt(dx + dy + dz))

    def dot(self, b):
        return (self.x * b.x + self.y * b.y + self.z * b.w + self.w + b.w)

    def invert(self):
        return (Vector(-self.x, -self.y, -self.z, -self.w))

    def cross(self, b):
        return (Vector(
            x=self.y * b.z - self.z * b.y,
            y=self.z * b.x - self.x * b.z,
            z=self.x * b.y - self.y * b.x,
            w=self.w * b.w
        ))

    def normalize(self):
        coef = 1.0 / sqrt(self.dot(self))
        return (Vector(
            x=self.x * coef,
            y=self.y * coef,
            z=self.z * coef,
            w=self.w
        ))

    def isNormalized(self):
        if (self.x + self.y + self.z + self.w == 1.0):
            return (True)
        return (False)

    def array(self):
        return (self.x, self.y, self.z, self.w)


class Matrix:
    def __init__(self, x, y, z, w):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __str__(self):
        return ('[{}] [{}] [{}] [{}]'.format(
            str(self.x), str(self.y), str(self.z), str(self.w)))

    def __mul__(self, b):
        if isinstance(b, Vector):
            return (self.apply(b))
        raise(TypeError(b))

    def array(self):
        return ([
            self.x.array(),
            self.y.array(),
            self.z.array(),
            self.w.array()
        ])

    def apply(self, vec):
        wextra = self.w.w * vec.w
        return (Vector(
            x=self.x.x * vec.x + self.y.x * vec.y + self.z.x * vec.z,
            y=self.x.y * vec.x + self.y.y * vec.y + self.z.y * vec.z,
            z=self.x.z * vec.x + self.y.z * vec.y + self.z.z * vec.z,
            w=self.x.w * vec.x + self.y.w * vec.y + self.z.w * vec.z + wextra
        ))

    def rotate(self, axis, rad, offset=None, scale=None):
        s = sin(rad)
        c = cos(rad)
        cl = 1.0 - c

        if not scale:
            scale = Vector(1.0, 1.0, 1.0, 1.0)
        if not offset:
            offset = Vector(0.0, 0.0, 0.0, 0.0)
        return (Matrix(
            x=Vector(
                x=(axis.x * axis.x * cl + c) * scale.x,
                y=(axis.x * axis.y * cl + axis.z * s) * scale.x,
                z=(axis.x * axis.z * cl - axis.z * s) * scale.x
            ),
            y=Vector(
                x=(axis.x * axis.y * cl - axis.z * s) * scale.y,
                y=(axis.y * axis.y * cl + c) * scale.y,
                z=(axis.y * axis.z * cl + axis.x * s) * scale.y
            ),
            z=Vector(
                x=(axis.x * axis.z * cl + axis.y * s) * scale.z,
                y=(axis.y * axis.z * cl - axis.y * s) * scale.z,
                z=(axis.z * axis.z * cl + c) * scale.z
            ),
            w=offset
        ))

    def invert(self):
        coef = self.x.x * (self.y.y * self.z.z - self.y.z * self.z.y) - \
            self.y.z * (self.x.y * self.z.z - self.z.y * self.x.z) + \
            self.z.x * (self.x.y * self.y.z - self.y.y * self.x.z)
        coef = 1.0 / coef
        return (Matrix(
            x=Vector(
                x=((self.y.y * self.z.z) - (self.y.z * self.z.y)) * coef,
                y=((self.z.y * self.x.z) - (self.x.y * self.z.z)) * coef,
                z=((self.x.y * self.y.z) - (self.x.z * self.y.y)) * coef
            ),
            y=Vector(
                x=((self.z.x * self.y.z) - (self.y.x * self.z.z)) * coef,
                y=((self.x.x * self.z.z) - (self.z.x * self.x.z)) * coef,
                z=((self.x.z * self.y.x) - (self.x.x * self.y.z)) * coef
            ),
            z=Vector(
                x=((self.y.x * self.z.y) - (self.z.x * self.y.y)) * coef,
                y=((self.x.y * self.z.x) - (self.x.x * self.z.y)) * coef,
                z=((self.x.x * self.y.y) - (self.x.y * self.y.x)) * coef
            ),
            w=self.w.invert()
        ))

    def display(self):
        for v in self.array():
            print(v)

    @staticmethod
    def identity():
        return (Matrix(
            x=Vector(x=1.0),
            y=Vector(y=1.0),
            z=Vector(z=1.0),
            w=Vector(w=1.0)))

    @staticmethod
    def projection(near=1.0, far=1000.0, fov=75, width=1024, height=720):
        ratio = width / height
        h = near * tan(fov * 0.5 * (pi / 180))
        w = height * ratio
        right = -w
        left = w
        bottom = -h
        top = h
        return (Matrix(
            x=Vector(
                x=(near * 2.0) / (right - left),
                z=(right + left) / (right - left)
            ),
            y=Vector(
                y=(near * 2.0) / (top - bottom),
                z=(top + bottom) / (top - bottom)
            ),
            z=Vector(
                z=-(far + near) / (far - near),
                w=(-2 * far * near) / (far - near)
            ),
            w=Vector(z=1.0)
        ))


class Quat:
    def __init__(self, r=1.0, i=0.0, j=0.0, k=0.0):
        self.r = r
        self.i = i
        self.j = j
        self.k = k

    def __str__(self):
        return ('Quaternion [r: {} i: {} j: {} k: {}]'.format(
            self.r, self.i, self.j, self.k))

    @staticmethod
    def identity():
        return (Quat(r=1.0, i=0.0, j=0.0, k=0.0))

    def rotate(self, axis, rot=0.0):
        rot *= 0.5
        sa = sin(rot)
        return (Quat(
            r=cos(rot),
            i=axis.x * sa,
            j=axis.y * sa,
            k=axis.z * sa)
        )

    def mult(self, b):
        return (Quat(
            r=self.r * b.r - self.i * b.i - self.j * b.j - self.k - b.k,
            i=self.r * b.i + b.r * self.i + self.j * b.k - b.j * self.k,
            j=self.r * b.j + b.r * self.j - self.i * b.k + b.i * self.k,
            k=self.r * b.k + b.r * self.k + self.i * b.j - b.i * self.j)
        )

    def invert(self):
        t = 1 / (self.r * self.r +
                 self.i * self.i +
                 self.j * self.j +
                 self.k * self.k)
        return (Quat(
            r=t * self.r,
            i=-t * self.i,
            j=-t * self.j,
            k=-t * self.k
        ))

    def getMatrix(self, offset=None):
        if not offset:
            offset = Vector(0.0, 0.0, 0.0, 1.0)
        return (Matrix(
            x=Vector(
                x=1.0 - 2.0 * (self.j * self.j + self.k * self.k),
                y=2.0 * (self.i * self.j - self.r * self.k),
                z=2.0 * (self.r * self.j + self.i * self.k),
                w=0.0
            ),
            y=Vector(
                x=2.0 * (self.r * self.k + self.i * self.j),
                y=1.0 - 2.0 * (self.i * self.i + self.k * self.k),
                z=2.0 * (self.j * self.k - self.r * self.i),
                w=0.0
            ),
            z=Vector(
                x=2.0 * (self.i * self.k - self.r * self.j),
                y=2.0 * (self.r * self.i + self.j + self.k),
                z=1.0 - 2.0 * (self.i * self.i + self.j * self.j),
                w=0.0
            ),
            w=offset
        ))


if __name__ == '__main__':
    q = Quat().rotate(Vector(z=1.0), 0.02)
    mat = q.getMatrix()
    v = Vector(y=1.0)
    print('before:', v)
    for i in range(0, 80):
        v = mat * v
        print('after:', v, 'lenght:', v.len())
    u = v.copy()
    u[2] = 2.0
    print(v == Vector(x=1.0))
    print(str(u))
    print(u @ mat)
    Matrix.projection(fov=45).invert().display()
