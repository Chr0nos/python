# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    norminette.py                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: snicolet <snicolet@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/21 20:45:31 by snicolet          #+#    #+#              #
#    Updated: 2016/09/23 02:14:32 by snicolet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import re
import os.path
import sys

class NormStat(object):
	def __init__(self, filepath):
		self._errors = list()
		self._count = {
			'len': 0,
			'function': 0,
			'lines': 0
		}
		self._filepath = filepath
		self._line = None
		self._allLines = list()
		self._headerRegex = {
			'sh': re.compile(r'#\s\*{76}\s#\n#\s{78}#\n#\s{57}:{3}\s{6}:{8}\s{4}#\n#\s{4}\w+.\w+\s{38}:\+:\s{6}:\+:\s{4}:\+:\s{4}#\n#\s{53}\+:\+\s\+:\+\s{9}\+:\+\s{6}#\n#\s{4}By:\s\w+\s<\w+@\w+.\w+.\w+>\s+\+#\+\s{2}\+:\+\s{7}\+#\+\s{9}#\n#\s{49}(\+#){5}\+\s{3}\+#\+\s{12}#\n#\s{4}Created:\s\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}:\d{2}\sby\s\w+\s{10}#\+#\s{4}\#\+#\s{14}#\n#\s{4}Updated:\s\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}:\d{2}\sby\s\w+\s{9}#{3}\s{3}#{8}\.fr\s{8}#\n#\s{78}#\n#\s\*{76}\s#'),
			'c': re.compile(r'/\*\s\*{74}\s\*\/\n\/\*\s{76}\*\/\n/\*\s{56}:{3}\s{6}:{8}\s{3}\*/\n/\*\s{3}\w+.c\s{45}:\+:\s{6}:\+:\s{4}:\+:\s{3}\*/\n/\*\s{52}(\+:\+\s){2}\s{8}\+:\+\s{5}\*/\n/\*\s{3}By:\s\w+\s<\w+@\w+\.\w+\.\w+>\s{10}\+#\+\s{2}\+:\+\s{7}\+#\+\s{8}\*/\n/\*\s{48}(\+#){5}\+\s{3}\+#\+\s{11}\*/\n/\*\s{3}Created:\s\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\sby\s\w+\s{10}#\+#\s{4}#\+#\s{13}\*/\n/\*\s{3}Updated:\s\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\sby\s\w+\s{9}#{3}\s{3}#{8}\.fr\s{7}\*/\n/\*\s{76}\*/\n/\*\s\*{74}\s\*/\n')
		}
		match = re.search('\.([ch])$', filepath)
		if match:
			self._ext = match.group(1)
		else:
			self._ext = False

	def checkAll(self):
		self.checkHeader()
		self.checkLineLen()
		self.checkFunctions()
		return (self)

	def checkFunctions(self):
		regexFuncs = re.compile(r"\w+[\w\s]+\((.+)\)")
		for line in self._allLines:
			x = regexFuncs.match(line)
			if x:
				print x.group(0)

	def checkHeader(self):
		"""
		return a boolean, true if the file has a valid header, False otherwise
		"""
		if not self._ext:
			return (True)
		if not self._ext in self._headerRegex:
			print("file type ." + self._ext + " does not require a header")
			return (True)
		header = "".join(self._allLines[:12])
		if not self._headerRegex['c'].match(header):
			print("norme error: missing header")
			return (False)
		return (True)

	def checkLineLen(self):
		"""
		return True if the line is bellow or equal to 80 chars
		in other cases False
		"""
		ret = True
		linelen = len(self._line)
		if linelen > 80:
			self._errors.append(str(self._lineNum) + " line is too long: " \
				+ str(linelen) + " caracters")
			ret = False
		if linelen > self._count['len']:
			self._count['len'] = linelen
		return (ret)

	def reIndent(self, indentSpace, line):
		"""
		remplace all the \t in start of the line by four spaces
		return the line in the new format
		"""
		u = indentSpace.search(line)
		if u:
			line = line[:-1].replace("\t", "    ", u.end())
		else:
			line = line[:-1]
		return (line)

	def __parseRawLines(self):
		"""
		remove all indentation from allLines and store it in self._rawLines
		return the list of parsed lines too
		"""
		self._rawLines = list()
		skip = re.compile("^\s+")
		for line in self._allLines:
			self._rawLines.append(skip.sub('', line[0:-1]))
		return (self._rawLines)

	def parse(self):
		"""
		main parsing function for the norminette
		return the object itself
		"""
		if not os.path.exists(self._filepath):
			self._errors.append("no such file or directory")
			return (False)
		indentSpace = re.compile("^\t+")
		self._lineNum = 0
		with open(self._filepath) as f:
			for line in f.readlines():
				#recherche des tabulations pour remplacement par des espaces
				self._lineNum += 1
				self._line = self.reIndent(indentSpace, line)
				self._allLines.append(line)
		self._lines = self._lineNum
		self.__parseRawLines()
		return (self)

	def showStats(self):
		print("max line lenght: " + str(self._count['len']))
		print("lines: " + str(self._lines))
		return (self)

class Norminette(object):
	def __init__(self):
		self.__files = list()
		self.__result = list()
	def getFiles(self):
		return (self.__files)
	def addFile(self, filepath):
		"""
		append "filepath" to the joblist, even if the file don't exists
		"""
		nfile = NormStat(filepath)
		self.__files.append(nfile)
		return (nfile)

if __name__ == "__main__":
	argc = len(sys.argv)
	if argc < 2:
		sys.exit(0)
	n = Norminette()
	del sys.argv[0]
	for filepath in sys.argv:
		print("Adding " + filepath)
		n.addFile(filepath).parse().showStats().checkAll()
