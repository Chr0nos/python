# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    sudoku.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: snicolet <snicolet@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/18 14:58:33 by snicolet          #+#    #+#              #
#    Updated: 2016/09/20 01:21:01 by snicolet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys

def ftSplitN(str, n):
	"""
	@return a splited tab
	this function split str every N caracters
	"""
	tab = []
	while str:
		tab.append(str[:n])
		str = str[n:]
	return (tab)

class Sudoku(object):
	def __init__(self):
		self._grid = False
		self._gridHeight = 9
		self._gridWidth = 9
		self.blocksize = 3
		self._soluceOne = False

	def __str__(self):
		if self.isGridSet():
			return (self._grid)
		return (False)

	def _checkGridLine(self, line):
		if type(line) is not str:
			return (False)
		digits = []
		valids = "0123456789"
		for c in line:
			if c not in valids:
				return (False)
			if c in digits:
				return (False)
			if c != '0':
				digits.append(c)
		return (True)

	def getGridColumn(self, grid, idx):
		"""
		return the "idx" column in grid as a string
		"""
		column = ""
		while idx < 81:
			column += grid[idx]
			idx += 9
		return (column)

	def getGridBlock(self, grid, pos):
		"""
		@return a boolean
		this function check the vality of the bloc at pos
		"""
		blocx = int(int(pos % 9) / 3)
		blocy = int(int(pos / 9) / 3)
		line = ""
		x = blocx * 3 + (blocy * 3 * 9)
		for p in range(3):
			line += grid[x:x + 3]
			x += 9
		return (line)

	def checkGrid(self, usergrid):
		"""
		this function check if the grid is valid
		"""
		if len(usergrid) != 81:
			return (False)
		lines = ftSplitN(usergrid, self._gridWidth);
		for line in lines:
			if not self._checkGridLine(line):
				return (False)
		#cree une liste sur tous les elements de la usergrid
		#pour check les colones nous alons les transformer en lignes pour que
		#check line s en occupe
		columns = [self.getGridColumn(usergrid, x) for x in range(0,8)]
		for col in columns:
			if not self._checkGridLine(col):
				return (False)
		return (True)

	def setGrid(self, usergrid):
		"""
		@param: usergrid is a 81 linear string
		@return int
		this function set the usergrid as the current grid if it's valid
		return a boolean: True in case of success, otherwise False
		"""
		if self.checkGrid(usergrid):
			self._grid = usergrid
			return (True)
		print("invalid grid")
		return (False)

	def showGrid(self):
		"""
		this function display the current grid
		"""
		if len(self._grid) != 81:
			return
		lines = ftSplitN(self._grid, self._gridWidth)
		lineNum = 0
		for line in lines:
			line = line.replace('0', '.')
			if lineNum % 3 == 0 and lineNum != 0:
				print '----|-----|----'
			print(line[:3] + ' | ' + line[3:6] + ' | ' + line[6:9])
			lineNum += 1
		print("")

	def isSafe(self, index, num):
		"""
		check if the index position is ok to receive "num"
		return True if it is, otherwise false
		"""
		if self._grid[index] != '0':
			return (False)
		posy = index - (index % 9)
		tab = self._grid[posy:posy + 9] + \
			self.getGridColumn(self._grid, index) + \
			self.getGridBlock(self._grid, index)
		if str(num) not in tab:
			return (True)
		return (False)

	def isGridSet(self):
		"""
		return True if the grid is set, otherwise False
		"""
		if not self._grid:
			return (False)
		return (True)

	def getNextEmptyIndex(self):
		"""
		return the next empty index in the current grid
		if the return is == 81 then the grid is already full
		"""
		index = 0
		while index < 81 and self._grid[index] != '0':
			index += 1
		return (index)

	def _solve(self, index, revert):
		"""
		find the solution in the specified order
		@param: index = int
		@param: revert = bool
		"""
		if index == 81:
			return (True)
		r = range(1, 10)
		if revert:
			r.reverse()
		for num in r:
			if self.isSafe(index, num):
				self._grid = self._grid[:index] + str(num) + self._grid[index + 1:]
				if self._solve(self.getNextEmptyIndex(), revert):
					return (True)
				self._grid = self._grid[:index] + '0' + self._grid[index + 1:]
		return (False)

	def solveMulti(self):
		"""
		find if the grid has at last one solution (but can be more)
		return Boolean
		"""
		if not self.isGridSet:
			return (False)
		return (self._solve(self.getNextEmptyIndex(), False))

	def solve(self):
		"""
		find if the grid has an UNIQUE solution
		if not False will be returned, then True
		"""
		if not self.isGridSet:
			return (False)
		self._gridOriginal = self._grid
		index = self.getNextEmptyIndex()
		if not self._solve(index, False):
			return (False)
		self._soluceOne = self._grid
		self._grid = self._gridOriginal
		self._solve(index, True)
		if self._soluceOne != self._grid:
			self._grid = self._gridOriginal
			return (False)
		return (True)

def solveSudoku(grid):
	u = Sudoku()
	u.setGrid(grid)
	#u.showGrid()
	#u.solveMulti()
	u.solve()
	u.showGrid()

if __name__ == "__main__":
	if len(sys.argv) != 2:
		tests = [
			"800650020050400009600020504047800090200704003090006410905070002700003060080062005",
			"305420810487901506029056374850793041613208957074065280241309065508670192096512408",
			"000000000000000000000000000000000000000000000000000000000000000000000000000000000"
		]
		for grid in tests:
			solveSudoku(grid)
	else:
		solveSudoku(sys.argv[1])
