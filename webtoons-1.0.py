#!/usr/bin/python3
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    webtoons.py                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: snicolet <snicolet@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/19 23:54:34 by snicolet          #+#    #+#              #
#    Updated: 2016/09/21 00:32:52 by snicolet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

"""
this script download all the images of a Webtoons page into the current path
uasge: python webtoons.py <url> [target path]
if target path don't exists it will be created
"""

from lxml import html
import requests
import sys
import os.path
from os import listdir
import re
import pymongo
import time
from zipfile import ZipFile

class WebToons(object):
	def __init__(self):
		self._userAgent = "Mozilla/5.0 (X11; Linux x86_64) \
				AppleWebKit/537.36 (KHTML, like Gecko) \
				Chrome/53.0.2785.116 Safari/537.36"

	def makeFileName(self, count, neededlen):
		"""
		return a valid file name from int(count) in format: xxxx
		the extenssion is not provided by this function
		if count is too slow zeroes will be added until neededlen is reached
		"""
		line = str(count)
		size = len(line)
		while size < neededlen:
			line = '0' + line
			size += 1
		return (line)

	def __flush(self, filename, data):
		"""
		write "data " in filename
		"""
		f = open(filename, 'wb')
		f.write(data)
		f.close()

	def _checkUrl(self, url):
		"""
		check for the validity of url, returns a boolean
		"""
		if not url.startswith("http://"):
			return (False)
		return (True)

	def __prepairDir(self, path):
		"""
		make the path valid if it dosent exists,
		if the path already exists the function does nothink
		"""
		if not os.path.exists(path):
			print("making directory " + path)
			os.mkdir(path)

	def __fetchPage(self, url):
		"""
		return the page object, quit in case of error
		"""
		if not self._checkUrl(url):
			print("invalid url")
			return (False)
		page = requests.get(url)
		if not page.ok:
			print("error to get main page")
			for line in page.headers:
				print(line + ": " + page.headers[line])
			return (False)
		return (page)

	def fetchPageContent(self, url, path):
		"""
		fetch all images of the url page in the "path" dir (must exists)
		url must start with http://
		"""
		page = self.__fetchPage(url)
		if not page:
			return (False)
		self.__prepairDir(path)
		tree = html.fromstring(page.content)
		pictures = tree.xpath('//div[@id="_imageList"]/img/@data-url')
		count = 0
		for img in pictures:
			filename = path + self.makeFileName(count, 4) + '.jpg'
			if os.path.exists(filename):
				print("skipping " + filename)
				count += 1
				continue
			print("fetch : " + img + " to -> " + filename)
			pictureData = requests.get(img,
				headers={'referer': url, 'User-Agent': self._userAgent})
			if not pictureData.ok:
				print("error on " + filename)
				return (False)
			self.__flush(filename, pictureData.content)
			count += 1
		return (True)

	def fetchAllPages(self, url, path, chapter = './'):
		"""
		fetch all chapters related to a scan and put them in the good folder
		chapter is the starting chapter
		path is the destination folder
		"""
		self.__prepairDir(path)
		if not path.endswith('/'):
			path += '/'
		p = re.compile("episode_no=(\d+)")
		realUrl = p.sub("episode_no=" + str(chapter), url)
		print(realUrl)
		while self.fetchPageContent(realUrl, path + \
			self.makeFileName(chapter, 3) + "/"):
			chapter += 1
			realUrl = p.sub("episode_no=" + str(chapter), url)
			print(realUrl)
		print("finished at chapter " + str(chapter))
		return (chapter)

# test url: http://www.webtoons.com/en/fantasy/the-gamer/season-3-ep-10/viewer?title_no=88&episode_no=134

class WebToonsHelper(object):
	def __init__(self):
		self.db = None

	def __del__(self):
		self.disconnect()

	def connect(self):
		try:
			self.db = pymongo.MongoClient('localhost', 27017)
		except pymongo.errors.ConnectionFailure:
			print('failed to connect to database.')
			sys.exit(1)

	def disconnect(self):
		if self.db:
			self.db.close()

	def bdd(self):
		if not self.db:
			self.connect()
		return (self.db.toons.items)

	def subscribe(self, name, root):
		search = {
			'$or': [
				{'name': name},
				{'root': root}
			]
		}
		target = self.bdd()
		if target.find_one(search):
			print('error: {} already subcribed'.format(name))
		else:
			fields = {
				'name': name,
				'root': root,
				'date': time.time(),
				'chapter': 1,
				'page': 0
			}
			target.insert_one(fields)
			print('{} subscribed'.format(name))

	def remove(self, name):
		if self.bdd().remove({'name': name}):
			print('{} removed'. format(name))
		else:
			print('error: {} is not subscribed'.format(name))

	def pull(self, name):
		db = self.bdd()
		target = db.find_one({'name': name})
		if target:
			u = WebToons()
			path = './{}/'.format(name)
			chapter = u.fetchAllPages(target['root'], path, int(target['chapter']))
			db.update_one({'name': name}, {'$set': {'chapter': chapter}})
		else:
			print('error: {} not subscribed'.format(name))

	def pullall(self):
		for x in self.bdd().find():
			self.pull(x['name'])

	def ls(self):
		print('subscribed scans:')
		for x in self.bdd().find():
			print('{} --- chapitre: {}'.format(x['name'], x['chapter']))

	def step(self, name):
		if self.bdd().update({'name': name}, {'$inc': {'chapter': 1}}):
			print('Chappitre saute')
		else:
			print('nom introuvable')

	def setchapter(self, name):
		try:
			self.bdd().update({'name': name},
				{'$set': {'chapter': int(sys.argv[3])}})
			print('set ok')
		except IndexError:
			print('chapitre manquant')
		except ValueError:
			print('chapitre invalide')

	def cbz(self, name):
		content = listdir('./{}/'.format(name))
		cbzRoot = '{}/cbz'.format(name)
		if not os.path.exists('{}'.format(cbzRoot)):
			os.mkdir(cbzRoot)
		for d in content:
			if d == 'cbz':
				continue
			path = '{}/{}/'.format(name, d)
			if not os.path.isdir(path):
				continue
			cbz = '{}/{}.cbz'.format(cbzRoot, d)
			if os.path.exists(cbz):
				continue
			with ZipFile(cbz, 'w') as z:
				count = 0
				for jpg in listdir(path):
					fullJpg = path + '/' + jpg
					# print('adding {}'.format(fullJpg))
					z.write(fullJpg)
					count += 1
				z.close()
				print('{} : ok ({} files)'.format(cbz, str(count).zfill(2)))

	def clean(self, name):
		content = listdir('./{}/'.format(name))
		noclean = False
		for dir in content:
			if dir == 'cbz':
				continue
			cbzfile = './{}/cbz/{}.cbz'.format(name, dir)
			if not os.path.exists(cbzfile):
				print('cbz file not found: {}'.format(cbzfile))
				noclean = True
				continue
			cleanpath = './{}/{}'.format(name, dir)
			for file in listdir(cleanpath):
				fullpath = './{}/{}/{}'.format(name, dir, file)
				os.unlink(fullpath)
			if noclean == False:
				os.rmdir('./{}/{}'.format(name, dir))
			else:
				print('keeping directory {} for {} due to some cbz not found'.format(dir, name))
		print('ok')

	def cleanall(self):
		for item in self.bdd().find(None, {'name': 1}):
			name = item.get('name')
			print('cleaning {}'.format(name))
			self.clean(name)


def controler():
	try:
		h = WebToonsHelper()
		if sys.argv[1] == 'add':
			h.subscribe(sys.argv[2], sys.argv[3])
		elif sys.argv[1] == 'pull':
			h.pull(sys.argv[2])
		elif sys.argv[1] == 'pullall':
			h.pullall()
		elif sys.argv[1] == 'list':
			h.ls()
		elif sys.argv[1] == 'remove' or sys.argv[1] == 'rm':
			h.remove(sys.argv[2])
		elif sys.argv[1] == 'skip':
			h.step(sys.argv[2])
		elif sys.argv[1] == 'set':
			h.setchapter(sys.argv[2])
		elif sys.argv[1] == 'cbz':
			h.cbz(sys.argv[2])
		elif sys.argv[1] == 'clean':
			h.clean(sys.argv[2])
		elif sys.argv[1] == 'cleanall':
			h.cleanall()
		else:
			print('error: unknow command {}'.format(sys.argv[1]))
	except (KeyError, IndexError) as e:
		print('usage: ./webtoons.py command [name] [param]')
	del(h)

if __name__ == "__main__":
	# oldmain()
	controler()
