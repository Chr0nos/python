import requests
import os
import sys
import time


def hsize(size):
	units = ('b', 'K', 'M', 'G', 'T', 'E', 'P', 'Y')
	power = 0
	while size >= 1024:
		size /= 1024
		power += 1
	return ('{:-4}{}'.format(round(size, 2), units[power]))


def download_fromreply(reply, filepath, chunkSize):
	with open(filepath, 'ba') as fd:
		done = 0
		lastDone = 0
		startTime = time.clock()
		lastTime = startTime
		size = int(reply.headers.get('Content-Length', 0))
		for chunk in reply.iter_content(chunkSize):
			now = time.clock()
			fd.write(chunk)
			done += len(chunk)
			print('left: {:10} - percent: {:4}% - speed: {}'.format(
				size - done,
				round(done / size * 100.0, 2),
				hsize((size / (now - startTime)) / 8) + 'o/s'
			))
			lastDone = done
			lastTime = now
		fd.close()
		print('total file size:', hsize(done))


def download(filepath, url, resume=False, chunkSize=8192):
	if resume:
		try:
			start = os.stat(filepath).st_size
		except FileNotFoundError:
			start = 0
			print('nothing to resume, starting from the begining')
	else:
		start = 0
	print('starting at {} byte'.format(start))
	reply = requests.get(
		url,
		headers={'Range': 'bytes={}-'.format(start)},
		stream=True,
		verify=False)
	if reply.status_code == 404:
		print('file not found')
		return
	print(reply.status_code, reply.reason)
	download_fromreply(reply, filepath, chunkSize)
	print('download done:', filepath)


if __name__ == '__main__':
	if len(sys.argv) < 3:
		print('usage: {} [filepath] [url]'.format(sys.argv[0]))
	else:
		download(sys.argv[1], sys.argv[2], True)
